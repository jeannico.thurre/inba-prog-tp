package s09;

public class Ex0902 {
    public static int intSqrt(int nbr) throws Exception {
        if (nbr < 0) {
             throw new Exception("NegativeNumber!!");
        }
        
        int i = 1;
        while (i*i <= nbr) {
            i++;
        }
        return i-1;
    }

    public static void intArraySqrt(int[] array) {
        String out = "";
        for (int i = 0; i < array.length; i++) {
            out += "\"Index " + i + ", value " + array[i] + ", integer square root ";
            try {
                out += intSqrt(array[i]);
            } catch (Exception e) {
                out += "undefined";
            }
            out += "\"\n";
        }
        System.out.println(out);
    }

    public static void main(String[] args) {
        try {
            //System.out.println(intSqrt(0));
        }
        catch (Exception e) {
            System.out.println("ss");
        }

        int[] array = {16, 32, 1564, -4};

        intArraySqrt(array);
    }
}
