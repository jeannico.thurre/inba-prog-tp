package s09;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Ex0903 {
    static void readFirstLine(BufferedReader br) throws IOException {
        String s;
        s = br.readLine();
        if (!s.startsWith("/*")) {
            System.out.println(s) ;
        }
    }

    static void readOtherLines(BufferedReader br) throws IOException {
        String s;
        s = br.readLine();
        while (s != null) {
            System.out.println(s) ;
            s = br.readLine();
        }
    }

    static void processFile(String filename) throws IOException {
        BufferedReader br = null;
        br = new BufferedReader(new FileReader(filename));
        readFirstLine(br);
        readOtherLines(br);
        br.close();
    }

    public static void main(String[] args) {
        String filename = "LineKiller.java";
        try {
            processFile(filename);
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
            System.exit(-1);
        }
    }
}
