package s23;

// Remember: in cryptography, any "naive" realization like this one
// is useful for pedagogical goals only, and
// must _never_ be considered secure!

import java.security.SecureRandom;

import static s23.SomeMathUtils.*;

// Here we define a class which can represent a public key or a private key
// - note that they both share the same structure, and the same services
// (to transmit a confidential message you cipher with a public key,
// but ciphering with a private key is the basis for digital signature).
public class RsaKey implements CipheringKey {

  // TODO (fields + constructor)
  long e_or_d;
  long n;

  public RsaKey(long e_or_d, long n) {
    this.e_or_d = e_or_d;
    this.n = n;
  }

  // Returns a sequence of space-separated base10 numbers, like "345 772 67".
  // The first number (eg 345) will correspond to the encryption of the first
  // character, and so on for the second, third etc.
  @Override
  public String cipher(String msg) {
    StringBuilder s = new StringBuilder();
    for (int i = 0; i < msg.length(); i++) {
      int j =0;
      while (Math.pow(msg.charAt(i), j++) != Double.POSITIVE_INFINITY) {}
      s.append(SomeMathUtils.powerModD(msg.charAt(i), e_or_d, n));
      s.append(" ");
    }

    return s.toString();  // TODO
  }

  // Receives a sequence of space-separated base10 numbers, like "345 772 67"
  // Each number in this sequence must be extracted and decrypted back as one
  // character.
  @Override
  public String decipher(String encoded) {
    // encoded.split(" ") is useful here...
    String[] encoded_array = encoded.split(" ");
    StringBuilder s = new StringBuilder();
    for (int i = 0; i < encoded_array.length; i++) {
      long char_long = Long.parseLong(encoded_array[i]);
      char char_char = (char) SomeMathUtils.powerModD(char_long, e_or_d, n);
      s.append(char_char);
    }

    return s.toString();  // TODO
  }

  public static AsymmetricKeys newKeyPair() {
    SecureRandom rnd = new SecureRandom();
    // chosen so that if m=getKthPrimeNb(minKth), Character.MAX_VALUE < m*m
    final int minKth = 58;
    // chosen arbitrarily (but ensuring maxPrime*maxPrime doesn't overflow)
    final int maxNbOfPrimesChoices = 1000;
    int pIndex = minKth + rnd.nextInt(maxNbOfPrimesChoices);
    int qIndex = minKth + rnd.nextInt(maxNbOfPrimesChoices);
    long p = getKthPrimeNb(pIndex);
    long q = getKthPrimeNb(qIndex);
    long n = p * q;
    long t = (p - 1) * (q - 1);
    long e = aNumberCoPrimeWith(t);
    long d = moduloInverseNaive(e, t);
    // now the public key is (e, n) and the private key is (d, n)
    AsymmetricKeys asymmetricKeys = new AsymmetricKeys() {
      @Override
      public CipheringKey publicKey() {
        return new RsaKey(e,n);
      }

      @Override
      public CipheringKey privateKey() {
        return new RsaKey(d,n);
      }
    };
    return asymmetricKeys; // TODO
  }

  // Recomputes the private key from the public key
  public static CipheringKey hackedPrivateKey(CipheringKey publicKey) {
    if(!(publicKey instanceof RsaKey))
      throw new IllegalArgumentException("not an RSA key...");
    RsaKey keyPub = (RsaKey) publicKey;
    long e = ((RsaKey) publicKey).e_or_d;
    long n = ((RsaKey) publicKey).n;

    long p = 2;
    while (n %p != 0) { p++; } // n est divisible uniquement par le p ou le q de départ

    long q = n/p; // une fois trouvé on peut retrouver q
    long t = (p-1)*(q-1); // puis t

    long d = moduloInverseNaive(e, t); // puis le d

    return new RsaKey(d, n); // TODO
  }

}
