//==============================================================================
//  EIA-FR / Jacques Bapst
//==============================================================================
package s05;

public class Pg0503 {

  public static void main(String[] args) {
    double e = 1, i = 1, increment = 1, factorial  = 1;

    while (increment > Math.pow(10,-12)){
      factorial = i*factorial;
      increment = 1/factorial;
      e += increment;
      i++;
    }

    System.out.println("iterations : " + (i-1));
    System.out.println("e : " + e);
  }
}
