package s05;

import proglib.SimpleIO;

public class Pg0504 {
    public static void main(String[] args) {
        int number;
        do { //Force user to enter a number > 1
            number = SimpleIO.readInt("Entrer un nombre supérieur à 1");
        } while (number <= 1);

        String out = number + " = ";

        { //Find primary divisors
            int quotient = number; //quotient = result of number divided by each primary divisor found

            int divisor = 2;

            while (divisor <= quotient) {
                if ((quotient % divisor) == 0) {
                    if (quotient != number) { //prevent out 'x' for the first iteration
                        out += " x ";
                    }
                    out += divisor;

                    quotient = quotient / divisor;
                } else {
                    divisor++;
                }
            }
        }

        SimpleIO.display(out);
    }
}
