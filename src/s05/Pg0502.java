package s05;

import proglib.SimpleIO;

public class Pg0502 {
    public static void main(String[] args) {
        {
            int a = SimpleIO.readInt("Nombre de dents : ");
            int b = SimpleIO.readInt("Hauteur des dents : ");

            for (int nbrDents = 1; nbrDents <= a; nbrDents++) {
                for (int hauteur = 0; hauteur < b; hauteur++) {
                    for (int ligne = 0; ligne <= hauteur; ligne++) {
                        System.out.print("*");
                    }
                    System.out.println();
                }
            }
        }

    }


}
