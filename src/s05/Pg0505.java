package s05;

public class Pg0505 {
    public static void main(String[] args) {
        int compteur = 1;
        String resultat = "";
        for (int i = 50; i <= 1000; i++) {
            if (i % 5 == 0 && i % 6 == 0) {
                if ((compteur % 10) == 0) {
                    resultat += i + "\n";
                } else {
                    resultat += i + ", ";
                }
                compteur++;
            }
        }
        System.out.print(resultat.substring(0, resultat.length() - 2));

    }
}
