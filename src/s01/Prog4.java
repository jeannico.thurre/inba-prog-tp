package s01;

import proglib.SimpleIO;

public class Prog4 {

    public static void main(String[] args) {
        int n1, n2, yeardeath;

        n1 = SimpleIO.readInt("Inscris ton age :");
        n2 = SimpleIO.readInt("Inscris l'espérance de vie en Suisse :");

        yeardeath = n2 - n1;

        SimpleIO.display("Tu vas mourir dans " + yeardeath + " ans.");
    }

}
