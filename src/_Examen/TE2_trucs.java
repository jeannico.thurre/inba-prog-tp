package _Examen;

public class TE2_trucs {
    public static void main(String[] args) {
        char c = 'c';
        System.out.println(c == 25);
    }

}

interface Ex5_a {
    //retour = type nombre
    //a = boolean
    //b = type nombre
    public abstract int test(boolean a, int b);
}

interface Ex5_b {
    //retour = String
    //a = boolean
    //b = boolean
   public abstract String test(boolean a, boolean b);
}

interface Ex5_c {
    //retour = boolean
    //a = String
    //b = tableau de nombres entiers
    public abstract boolean test(String a, char[] b);
}


interface Ex5_d {
    //retour = void
    public abstract void test();
}

interface Ex5_e {
    //retour = double ou float
    //a = tableau 3D de n'importe
    //b = nombre entier
    //c = truc parsable
    public abstract double test(int[][][] a, int b, String c);
}


