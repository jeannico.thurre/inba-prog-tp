package _Examen;

public class Tests {
    public static void main(String[] args) {

    }
    public abstract class Animal {
        int nbrPattes;

        public Animal(Animal a) {
            System.out.println(a.nbrPattes);
        }

        abstract void manger();
    }

    public class Chat extends Animal {
        public Chat(Animal a) {
            super(a);
        }

        @Override
        void manger() {
            System.out.println("Je mange avec mes " + this.nbrPattes + " pattes");
        }
    }
}
