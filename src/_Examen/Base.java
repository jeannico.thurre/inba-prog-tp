package _Examen;

public class Base {
    public boolean equals(Object obj) {
        if (this == obj) {
            System.out.println("return #1 ");
            return true;
        }
        if (obj instanceof Base) {
            System.out.println("return #2");
            return true;
        }
        System.out.println("return #3");
        return false;
    }
}
