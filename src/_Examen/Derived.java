package _Examen;

public class Derived extends Base {
    private int x;

    public Derived(int x) {
        this.x = x;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            System.out.println("return #4");
            return true;
        }
        if (obj instanceof Derived) {
            Derived other = (Derived) obj;
            if (x != other.x) {
                System.out.println("return #5");
                return false;
            }
        }
        System.out.println("return #6");
        return super.equals(obj);
    }

    public static void main(String[] args) {
        Derived d1 = new Derived(0);
        Derived d2 = new Derived(1);
        Base b1 = new Base();
        Base b2 = new Base();
        Base x1 = d1;

        //bloc 1
        if (d1 == d2) {
            System.out.println("d1 == d2");
        }

        if (x1.equals(d1)) {
            System.out.println("x1.equals(d1)");
        }

        if (d1.equals(b1)) {
            System.out.println("d1.equals(b1)");
        }

        if (d1.equals(d2)) {
            System.out.println("d1.equals(d2)");
        }

        if (d1.equals(null)) {
            System.out.println("d1.equals(null)");
        }

        if (b1.equals(b2)) {
            System.out.println("b1.equals(b2)");
        }
    }
}
