package s06;

public class Pg0606 {
    public static int sumDigits(int number) {
        int sum = 0;

        int remainingDigits = number;

        while (remainingDigits/10.0 > 0.0) {
            //use cast to keep only last digit
            double lastdigit = ((double) remainingDigits/10 - remainingDigits/10) * 10;
            sum += lastdigit;
            remainingDigits = remainingDigits/10;
        }

        return sum;
    }

    public static void main(String[] args) {
        System.out.println(sumDigits(12345));
    }
}
