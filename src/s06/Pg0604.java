package s06;

public class Pg0604 {
    public static int getPairs(int p) {
        int count = 0;
        int b = 1;
        while (b < p) {
            int a = 1;
            while (a < b) {
                if ((a*a + b*b + 1)%(a*b) == 0) {
                    System.out.println("("+ a +","+ b +")");
                    count++;
                }
                a++;
            }
            b++;
        }
        return count;
    }

    public static void main(String[] args) {
        System.out.println("nombre de paires : " + getPairs(1000));
    }
}
