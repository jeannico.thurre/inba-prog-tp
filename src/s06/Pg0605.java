package s06;

public class Pg0605 {
    public static int getGCD(int x, int y) {
        int GCD = 0;
        int min = 0;

        if (x < y) {
            min = x;
        } else if (y < x) {
            min = y;
        } else {
            GCD = x;
        }

        int i = 1;
        while (i <= x && i <= y) {
            if (x%i==0 && y%i==0) {
                GCD = i;
            }
            i++;
        }

        return GCD;
    }

    public static void main(String[] args) {
        System.out.println(getGCD(60606,8554));
    }
}
