package s17.moon;

public class CrashAtLauchException extends Throwable {
    public CrashAtLauchException() {
        super();
    }
    public CrashAtLauchException(String message) {
        super(message);
    }
}
