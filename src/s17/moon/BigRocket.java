package s17.moon;

import java.util.Random;

public class BigRocket extends Rocket {
    public BigRocket() {
        super();
        this.weight_empty = 18_000;
        this.weight_full = 29_000;
        this.price_launch = 130;
    }

    public void launch() throws CrashAtLauchException {
        double temp = getWeight_items();
        double crash_probability_actual = (double) getWeight_items()/(weight_full-weight_empty) * 4;
        if (new Random().nextDouble(100) < crash_probability_actual) {
            throw new CrashAtLauchException();
        }
    }
}
