package s17.moon;

import java.util.Random;

public class SmallRocket extends Rocket {
    public SmallRocket() {
        super();
        this.weight_empty = 10_000;
        this.weight_full = 18_000;
        this.price_launch = 100;
    }

    public void launch() throws CrashAtLauchException {
        double crash_probability_actual = 0;
        for (int i = 0; i < items.length; i++) {
            if (crash_probability_actual != 5) {
                crash_probability_actual += 0.5;
            }
        }

        if (new Random().nextDouble(100) < crash_probability_actual) {
            throw new CrashAtLauchException();
        }
    }
}
