package s17.moon;

import java.util.Arrays;

// TODO (this is only a dummy skeleton)
// If a certain instance member is present in every subclass of Rocket, then it
// is probably a good idea to declare it in the base class...
public class Rocket {
  // TODO
  int weight_full;
  int weight_empty;
  int price_launch;
  Item[] items;

  public Rocket() {
    items = new Item[0];
    weight_full = 0;
    weight_empty = 0;
    price_launch = 0;
  }

  public int getWeight_items() {
    int weight_items = 0;
    for (int i = 0; i < items.length; i++) {
      weight_items += items[i].weight;
    }
    return weight_items;
  }

  public boolean canCarry(Item item) {
    int weight_actual = weight_empty + getWeight_items();
    //throw new UnsupportedOperationException(); // TODO
    return (weight_actual+item.weight) < weight_full;
  }

  public void carry(Item item) {
    //throw new UnsupportedOperationException(); // TODO
    items = Arrays.copyOf(items, items.length+1);
    items[this.items.length-1] = item;
  }

  public void launch() throws CrashAtLauchException {

  }

  public double cost() {
    return price_launch;
  }
}
