package s13;

public class Complex {
    public double x; //real
    public double y; //imaginary

    public Complex(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void add(Complex c) {
        this.x += c.x;
        this.y += c.y;
    }

    public Complex addOp(Complex c) {
        Complex result = new Complex(
                this.x + c.x,
                this.y + c.y);
        return result;
    }

    public void multiply(Complex c) {
        this.x = (this.x * c.x) - (this.y * c.y);
        this.y = (this.x * c.y) + (this.y * c.x);
    }

    public Complex multiplyOp(Complex c) {
        Complex result = new Complex(
                (this.x * c.x) - (this.y * c.y),
                (this.x * c.y) + (this.y * c.x));

        return result;
    }

    public double modulus() {
        double result = 0;
        result = this.x * this.x + this.y * this.y;
        result = Math.sqrt(result);
        return result;
    }

    public void display() {
        System.out.println("("+ this.x +")+(" + this.y + ")i");
    }
}
