
package s21;

public class Ex1_PlayWithSeries {
  
  public static abstract class Series {
    public abstract double function(int i);
    public double evaluateBetween(int from, int to) {
      double sum = 0;
      for (int i = from; i < to; i++) {
        sum += function(i);
      }
      return sum;
    }

  }
  //=================================================
  // static class SeriesB1 // TODO...
  public static class SeriesB1 extends Series {
    @Override
    public double function(int i) {
      return i/Math.pow(2,i);
    }
  }
  
  // static class SeriesB2 // TODO...
  public static class SeriesB2 extends Series {
    @Override
    public double function(int i) {
      return Math.pow(i,2)/Math.pow(2,i);
    }
  }
  
  //=================================================

  public static void main(String[] args) {
    // *******************************************
    // ****** With internal static classes: ******
    // *******************************************

    // TODO... create instances of SeriesB1/B2, evaluate and print the result
    //         when evaluating between 0 and 20
    //         expected results: 1.99… and 5.99…

    System.out.println(((new SeriesB1()).evaluateBetween(0,20)));
    System.out.println(((new SeriesB2()).evaluateBetween(0,20)));
    
    // *********************************************
    // ****** With anonymous classes: (ex. 3) ******
    // *********************************************

    // TODO... create equivalent instances, evaluate and print the result

    System.out.println(new Series() {
      @Override
      public double function(int i) {

        return 0;
      }
    }.evaluateBetween(0, 20));
  }
  
}
