
package s21;

import s13.Complex;

public class PlayWithFunctions {

  //=================================================
  // TODO ...
  // static class MySeriesFct ...          // i/2^i

  // TODO ...
  // static class MyTrigonometricFct ...   // sin^2(x) * cos(x)


  //=================================================
  static double foldLeft(INumberSequence ns, IFoldableOperation op) {
    double sum = op.initialValue();
    while (ns.hasMoreNumbers()) {
      sum += op.combine(sum, ns.nextNumber());
    }
    return sum; // TODO (Exercise s21.ex2b)
  }

  private static void computeAll(IFunction fa) { //, IFunction fb) {
    // TODO (Exercise s21.ex2d)
    //INumberSequence nsArray = ...    // 1.2, 3.4, 5.6    (Exercise s21.ex2d)
    //INumberSequence nsSeries =       // fa entre 0 et 20 (Exercise s21.ex2d)
    //INumberSequence nsSamples =      // fb entre 0 et PI avec 1000 échantillons (Exercise s21.ex2d, optional)

    //System.out.println(...   // le produit sur nsArray  (Exercise s21.ex2d)
    //System.out.println(...   // la somme sur nsSeries   (Exercise s21.ex2d)
    //System.out.println(...   // le maximum sur nsSample (Exercise s21.ex2d, optional)

    INumberSequence nsArray = CommonSequences.fromArray(new double[]{1.2, 3.4, 5.6});
    IFoldableOperation product_operation = (IFoldableOperation) FoldOperators.PRODUCT;
    System.out.println(foldLeft(nsArray, product_operation));

    INumberSequence nsSeries = (INumberSequence) CommonSequences.fromSeries(fa, 0, 20);
    IFoldableOperation sum_operation = (IFoldableOperation) FoldOperators.SUM;
    System.out.println(foldLeft(nsSeries, sum_operation));
  }

  static class fa implements IFunction {
    @Override
    public double valueAt(double x) {
      return x/Math.pow(2,x);
    }
  }
  public static void main(String[] args) {
    //IFunction fa=null, fb=null;
    // *******************************************
    // ****** With internal static classes: ******
    // *******************************************
    // fa = ...
    // fb = ...
    //computeAll(fa, fb);

    // *************************************
    // ****** With anonymous classes: ******
    // *************************************
    // fa = ...
    // fb = ...
    //computeAll(fa, fb);

    // **************************************
    // ****** With lambda expressions: ******  <----- Ce sera pour le TP S22 !!!
    // **************************************
    // fa = ...
    // fb = ...
    //computeAll(fa, fb);

    IFunction fa = new fa();
    computeAll(fa);
  }
}
