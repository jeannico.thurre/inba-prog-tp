package s21;

public interface IFoldableOperation {
    double initialValue();
    double combine(double accumulated,
                   double newValue);
}