
package s21;
import static s21.PlayWithFunctions.*;

public class CommonSequences {
  // ***********************************
  // ****** With internal classes ******
  // ***********************************
  
  public static INumberSequence fromArray(double[] t) {
    return new ArrayNumberSequence(t); // TODO ...  return new ArrayNumberSequence(t)
  }
  
  public static INumberSequence fromSeries(IFunction f, int from, int to) {
    return new SeriesNumberSequence(f, from, to); // TODO ...  return new SeriesNumberSequence(...)
  }
  
  public static INumberSequence fromSampledFunction(IFunction f,
                                                    double from, double to,
                                                    int nSubSamples) {
    return null; // TODO ... return new SampledFunctionNumberSequence(...)
  }
  
  // static class ArrayNumberSequence ...  // TODO (Exercise s21.ex2c)
  static class ArrayNumberSequence implements INumberSequence {
    double[] numbers;
    int actual_index;
    ArrayNumberSequence(double[] t) {
      numbers = t;
      actual_index = 0;
    }
    @Override
    public boolean hasMoreNumbers() {
      return (actual_index<numbers.length);
    }
    @Override
    public double nextNumber() {
      return numbers[actual_index++];
    }
  }
  
  // static class SeriesNumberSequence ...  // TODO (Exercise s21.ex2c)

  static class SeriesNumberSequence implements INumberSequence {
    double[] numbers;
    int actual_index;
    SeriesNumberSequence(IFunction f, int from, int to) {
      numbers = new double[to-from];
      for (int i = from; i < to; i++) {
        numbers[i] = f.valueAt(i);
      }
    }
    @Override
    public boolean hasMoreNumbers() {
      return (actual_index<numbers.length);
    }
    @Override
    public double nextNumber() {
      return numbers[actual_index++];
    }
  }

  // static class SampledFunctionNumberSequence ...  // TODO (Exercise s21.ex2c, optional part)
  
  // ************************************
  // ****** With anonymous classes ******
  // ************************************
  
  public static INumberSequence fromArray1(final double[] t) {
    return null; // TODO (Exercise s21.ex3, optional)
  }

  public static INumberSequence fromSeries1(final IFunction f,
                                            final int from, final int to) {
    return null; // TODO (Exercise s21.ex3, optional)
  }
  
  public static INumberSequence fromSampledFunction1(final IFunction f,
                                                     final double from,
                                                     final double to,
                                                     final int nSubSamples) {
    return null; // TODO (Exercise s21.ex3, optional)
  }
}
