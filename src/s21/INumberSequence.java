package s21;

public interface INumberSequence {
    boolean hasMoreNumbers();
    double nextNumber();
}