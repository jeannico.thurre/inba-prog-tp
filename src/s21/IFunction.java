package s21;

public interface IFunction {
    double valueAt(double x);
}
