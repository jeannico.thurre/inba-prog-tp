
package s21;

// Exercise s21.ex2a (and the optional s21.ex3)

public class FoldOperators {
  // ***********************************
  // ****** With internal classes ******
  // ***********************************

  //public static final IFoldableOperation SUM = new SumOperation();
  //public static final IFoldableOperation PRODUCT = new ...
  //public static final IFoldableOperation MAX =...
  public static final IFoldableOperation SUM = new SumOperation();
  public static final IFoldableOperation PRODUCT = new ProductOperation();
  public static final IFoldableOperation MAX = new SumOperation();

  //private static class SumOperation ...
  private static class SumOperation implements IFoldableOperation {
      @Override
      public double initialValue() {
          return 0;
      }
      @Override
      public double combine(double accumulated, double newValue) {
          return accumulated + newValue;
      }
  }

  //private static class ProductOperation ...
  private static class ProductOperation implements IFoldableOperation {
      @Override
      public double initialValue() {
          return 1;
      }
      @Override
      public double combine(double accumulated, double newValue) {
          return accumulated * newValue;
      }
  }

  //private static class MaxOperation ...
  private static class MaxOperation implements IFoldableOperation {
      @Override
      public double initialValue() {
          return Double.MIN_VALUE;
      }
      @Override
      public double combine(double accumulated, double newValue) {
          return Math.max(accumulated, newValue);
      }
  }


  // ********************************************
  // ****** With anonymous classes (ex. 3) ******
  // ********************************************

  //public static final IFoldableOperation SUM1 =...

  //public static final IFoldableOperation PRODUCT1 =...

  //public static final IFoldableOperation MAX1 =...


}