package s19;

public class Ex1 {
    public static class Collier { }
    public static class Animal {
        public Patte[] pattes;
        public Tête tête;
    }
    public static class Chien extends Animal {
        public Collier collier;
    }
    public static class Chat extends Animal {
        public Collier collier;
    }
    public static class Oiseau extends Animal {
        public Aile[] ailes;
    }
    public static class Pigeon extends Oiseau { }
    public static class Organe { }
    public static class Patte extends Organe { }
    public static class Tête extends Organe {
        public Oeil[] yeux;
    }
    public static class Oeil extends Organe { }
    public static class Aile extends Organe { }
}
