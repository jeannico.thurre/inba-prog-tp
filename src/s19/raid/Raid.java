
package s19.raid;

public abstract class Raid extends DiskDevice {
  protected final DiskDevice[] members;

  // Here we prefer to consider that every member has the same capacity
  protected final int commonMemberCapacity;

  protected Raid(DiskDevice[] members) {//throws Exception {
    /**for (int i = 0; i < members.length; i++) {
      for (int j = 0; j < members.length; j++) {
        if(i != j && members[i] == members[j]) {
          throw new Exception("disk " + i + "has the same reference as the disk " + j);
        }
      }
    }**/
    this.members = members;
    int c = 0;
    if(members.length > 0) {
      c = members[0].capacity();
      for(DiskDevice d: members)
        c = Math.min(c, d.capacity());
    }
    this.commonMemberCapacity = c;
  }
}