package s11;

public class Ex1103 {
    public static int returnSum(String s) {
        int result = 0;
        for (int i = 0; i < s.length(); i++) {
            result += (int) s.charAt(i);
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(returnSum("A"));
    }
}
