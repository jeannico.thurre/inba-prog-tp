package s11;

import proglib.SimpleIO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.StringTokenizer;

public class Ex1105 {
    public static void main(String[] args) {
        String filename;
        if (args.length == 0) {
            filename = SimpleIO.readString("Entrer le nom du fichier");
        } else {
            filename = args[0];
        }


        try (FileReader fp = new FileReader(filename);
             BufferedReader br = new BufferedReader(fp) ) {
            int lines = 0;
            int words = 0;
            int chars = 0;

            String line = br.readLine();
            while (line != null) {
                lines += 1;
                StringTokenizer t = new StringTokenizer(line);

                words += t.countTokens();
                while (t.hasMoreTokens()) {
                    String word = t.nextToken();
                    chars += word.length();
                }

                line = br.readLine();
            }

            System.out.println("Number of words "+words);
            System.out.println("Number of lines "+lines);
            System.out.println("Number of chars "+chars);
        } catch (Exception e) {
            System.out.println("Impossible de trouver le fichier "+filename);
        }
    }
}
