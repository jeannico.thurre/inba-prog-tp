//==============================================================================
//  HEIA-FR
//==============================================================================
package s16;

// TODO: add relevant modifiers (private, static, final, ...) to the declaration of the members

public class Circle {
  
  final static double  PI = 3.14159265358;
  private double radius;

  public Circle(double r) {
    this.radius = r;
  }

  public double perimeter (Circle c) {
    return 2*PI*radius;
  }
  
  public double area() {
    return PI*sqrRad();
  }
  
  public boolean isSmaller(Circle c) {
    return c.radius < radius;
  }

  public Circle fromArea(double area) {
    return new Circle(Math.sqrt(area/PI));
  }

  public double sqrRad() {
    return radius*radius;
  }
}
