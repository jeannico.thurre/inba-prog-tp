package s22;

public class Ex1EntityDemo {
  //======================================
  static class Entity {
    ValueChangedListener listener;
    String value;

    // TODO
    public Entity() {
      // TODO
      this.listener = null;
      this.value = "";
    }

    public String getValue() {
      return this.value; // TODO
    }

    public void setValue(String value) {
      // TODO
      this.value = value;
      if (listener != null) {
        listener.onValueChanged(this);
      }
    }

    public void setListener(ValueChangedListener listener) {
      // TODO
      this.listener = listener;
    }
  }
  //======================================
  public interface ValueChangedListener {
    void onValueChanged(Entity e);
    static void test(Entity e) {

    };
  }
  //======================================
  // TODO
  // static class MyListener ....
  static class MyListener implements ValueChangedListener {

    @Override
    public void onValueChanged(Entity e) {
      System.out.println("An entity changed its value to: " + e.value);
    }
  }

  static class MyIntrusiveListener implements ValueChangedListener {

    @Override
    public void onValueChanged(Entity e) {
      e.setValue("Apple");
      System.out.println("An entity changed its value to: " + e.value);
    }
  }
  
  //======================================
  public static void main(String[] args) {
    /* Please uncomment..*/
    Entity entity = new Entity();
    ValueChangedListener listener;

    //classe interne
    //listener = new MyListener();

    //classe anonyme
    /*listener = new ValueChangedListener() {
      @Override
      public void onValueChanged(Entity e) {
        System.out.println("An entity changed its value to: " + e.value);
      }
    };*/

    //lambda
    listener = (Entity e) -> System.out.println("An entity changed its value to: " + e.value);

    entity.setValue("abcd");
    entity.setListener(listener);
    entity.setValue("toto");
    entity.setValue("titi");
    //*/
  }
  
  /* Affichage lors de l'exécution : 
     An entity changed its value to: toto
     An entity changed its value to: titi
   */
}
