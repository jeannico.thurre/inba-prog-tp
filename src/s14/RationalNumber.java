//==============================================================================
//  EIA-FR
//==============================================================================
package s14;

public class RationalNumber {
  private double denominator;
  private double numerator;
  // should raise an IllegalArgumentException when denominator == 0
  public RationalNumber(int numerator, int denominator) throws IllegalArgumentException {
    if (denominator == 0) {
      throw new IllegalArgumentException();
    }
    this.numerator = numerator;
    this.denominator = denominator;
  }

  public void multiply(RationalNumber a) {
    this.numerator *= a.numerator;
    this.denominator *= a.denominator;
  }
  public void divide(RationalNumber a) {
    this.numerator /= a.numerator;
    this.denominator /= a.denominator;
  }
  public void add(RationalNumber a) {
    this.numerator = this.numerator*a.denominator + a.numerator*this.denominator;
    this.denominator *= a.denominator;
  }
  public void subtract(RationalNumber a) {
    this.numerator = this.numerator*a.denominator - a.numerator*this.denominator;
    this.denominator *= a.denominator;
  }
  public void multiply(int i){
    this.numerator *= i;
  }
  public void add (int i){
    this.numerator += this.denominator*i;
  }
  public boolean lessThan(RationalNumber a) {
    return (this.numerator*a.denominator < a.numerator*this.denominator);
  }
  public boolean equals(RationalNumber a) {
    return (this.numerator*a.denominator == a.numerator*this.denominator);
  }
  public float toFloat() {
    return (float) (this.numerator/this.denominator);
  }
  public String toString(){
    return this.numerator+"/"+this.denominator;
  }
}