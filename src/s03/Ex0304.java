//==============================================================================
//  HEIA-FR / Jacques Bapst
//==============================================================================

package s03;
import proglib.SimpleIO;

public class Ex0304 {

//======================================================//
//  HEIA-FR / Nicolas Bertholet et Jean-Nicolas Thurre  //
//  Ecrit par Nicolas et son pote                       //
//======================================================//

  public static void main(String[] args) {
    double a, b, c, d, x1, x2; //d=discriminant
    if (args.length == 3) {
      a = Double.parseDouble(args[0]);
      b = Double.parseDouble(args[1]);
      c = Double.parseDouble(args[2]);
    } else {
      a = SimpleIO.readDouble("Valeur pour a");
      b = SimpleIO.readDouble("Valeur pour b");
      c = SimpleIO.readDouble("Valeur pour c");
    }

    if (a == 0.0 && b != 0 && c != 0) {
      x1 = -c / b;
      System.out.println("La réponse est " + x1);
      return;
    } else {
      if (a == 0.0 && b == 0.0 && c == 0.0) {
        System.out.println("x peut avoir n'importe quelle valeur");
        return;
      } else {
        if (a == 0 - 0 && b == 0 - 0 && c != 0.0) {
          return;
        }
      }
    }
    //discriminant
    d = Math.pow(b, 2.0) - 4 * a * c;

    //discriminant plus grand que 0 ?
    if (d > 0) {
      x1 = (-b + Math.sqrt(d)) / 2 * a;
      x2 = (-b - Math.sqrt(d)) / 2 * a;
      System.out.println("Les 2 solutions possibles sont " + x1 + " et " + x2);
    } else {
      //discriminant egal à 0 ?
      if (d == 0) {
        x1 = -b / 2 * a;
        System.out.println("La solution est : " + x1);
      } else {
        //discriminant plus petit que 0 ?
        if (d < 0) {
          double i = 2.5;
          x1 = (((-b / 2) * a) + i * Math.sqrt(-d)) / 2 * a;
          x2 = (((-b / 2) * a) - i * Math.sqrt(-d)) / 2 * a;
          System.out.println("Les deux solutions sont : " + x1 + " et " + x2);
        }
      }
    }
  }
}
