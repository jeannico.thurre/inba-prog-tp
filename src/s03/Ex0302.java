package s03;

public class Ex0302 {
    public static void main(String[] args)  {
        //Ecrit par Nico
        int a = 0;
        int b = 0;
        int c = 0;
        int d = 0;
        int x = 0;

        if (a < b) {
            if (c < d) {
                x = 1;
            } else if (a < c) {
                // a < b et c >= d et a < c
                if (b < d) {
                    // a < b et b < d et a < c et c >= d     → a < b < d <= c
                    x = 2;
                } else {
                    // a < b et b >= d et a < c et c >= d     → a < d <= c,b
                    x = 3;
                }
            } else if (a < d) {
                // → le if (b > c) est contradictoire donc inutile, on pourrait remplacer a > d et enlever le else
                if (b > c) {
                    // a < b et c >= d et a >= c et b > c et a < d     → d <= c <= a < b → contradictoire
                    x = 4;
                } else {
                    // a < b et c >= d et a >= c et b <= c    → d <= c <= a < b / b <= c → contradictoire
                    x = 5;
                }
            } else {
                // a < b et c >= d et a >= c et a >= d        → d <= c <= a < b
                x = 6;
            }
        } else { // a >= b
            x = 7;
        }

        //Version simplifiée
        if (a < b) {
            if (c < d) {
                x = 1;
            } else if (a < c) {
                // a < b et c >= d et a < c
                if (b < d) {
                    // a < b et b < d et a < c et c >= d     → a < b < d <= c
                    x = 2;
                } else {
                    // a < b et b >= d et a < c et c >= d     → a < d <= c,b
                    x = 3;
                }
            } else if (a < d) {
                x = 5;
            } else {
                // a < b et c >= d et a >= c et a >= d        → d <= c <= a < b
                x = 6;
            }
        } else { // a >= b
            x = 7;
        }
    }
}
