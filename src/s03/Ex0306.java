package s03;

import proglib.SimpleIO;

public class Ex0306 {
    public static void main(String[] args) {
        int r;
        int a = SimpleIO.readInt("Nombre désiré pour a : ");
        int b = SimpleIO.readInt("Nombre désiré pour b : ");
        int c = SimpleIO.readInt("Nombre désiré pour c : ");

        if ((a > b) && (a > c)) {
            r = a;
        } else {
            if ((b > a) && (b > c)) {
                r = b;
            } else {
                r = c;
            }
        }
        System.out.println("La valeur la plus haute parmi a, b et c est : " + r);
    }

    public static void correction() {
        int r;
        int a = SimpleIO.readInt("Nombre désiré pour a : ");
        int b = SimpleIO.readInt("Nombre désiré pour b : ");
        int c = SimpleIO.readInt("Nombre désiré pour c : ");

        r = a;
        if (b > c && b > a) {
            r = b;
        } else if (c > a) {
            r = c;
        }
        System.out.println("La valeur la plus haute parmi a, b et c est : " + r);
    }

}
