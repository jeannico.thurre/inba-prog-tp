package s03;

import proglib.SimpleIO;

public class Ex0301f {
    public static void main(String[] args)  {
        int sidesnumber = 6;
        double longside = 0, othrsides = 0;
        {
            for (int i = 0; i < sidesnumber; i++) {
                double sidei = SimpleIO.readDouble("Entrer la taille du côté " + (i+1));
                if (sidei > longside) {
                    othrsides += longside;
                    longside = sidei;
                } else {
                    othrsides += sidei;
                }
            }
        }

        if (longside >  othrsides) {
            SimpleIO.display("Forme valide");
        } else {
            SimpleIO.displayError("Forme invalide");
        }
    }
}
