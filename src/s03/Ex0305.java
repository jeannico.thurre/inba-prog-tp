package s03;

import proglib.SimpleIO;

public class Ex0305 {
    public static void main(String[] args) {
        int a = SimpleIO.readInt("Entrer un nombre entier");

        boolean div5 = (a % 5) == 0;
        boolean div6 = (a % 6) == 0;

        String out = a + " is divisible by ";

        if (div5 && div6) {
            out += "5 and by 6";
        } else if (div6) {
            out += "6";
        } else if (div5) {
            out += "5";
        } else {
            out += "neither 5 nor 6";
        }

        SimpleIO.display(out);
    }
}
