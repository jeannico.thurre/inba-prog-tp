package s03;

import proglib.SimpleIO;

public class Ex0301 {
    public static void main(String[] args)  {
        double longside, side1, side2;
        int sidesnumber = 3;
        {
            double a = SimpleIO.readDouble("Entrer la taille du côté a");
            double b = SimpleIO.readDouble("Entrer la taille du côté b");
            double c = SimpleIO.readDouble("Entrer la taille du côté c");

            if ((a > b) && (b > c)) { // ((a < b) && (b >= c))
                longside = a;
                side1 = b;
                side2 = c;
            } else if (b >= c) {
                longside = b;
                side1 = a;
                side2 = c;
            } else {
                longside = c;
                side1 = a;
                side2 = b;
            }
        }

        if (longside > (side1 + side2)) { // (longside < (side1 + side2))
            SimpleIO.display("Triangle valide");
        } else {
            SimpleIO.displayError("Triangle invalide");
        }
    }
}
