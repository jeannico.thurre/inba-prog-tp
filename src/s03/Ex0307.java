package s03;

import proglib.SimpleIO;

public class Ex0307 {
    public static void main(String[] args) {
        int day = SimpleIO.readInt("Entrer un jour de la semaine (1 pour lundi, 2 pour mardi, etc.)");

        String out = "";

        { // (a) with switch
            switch (day) {
                case 1:
                    out = "Working day";
                    break;
                case 2:
                    out = "Working day";
                    break;
                case 3:
                    out = "Working day";
                    break;
                case 4:
                    out = "Working day";
                    break;
                case 5:
                    out = "Working day";
                    break;
                case 6:
                    out = "Week-end";
                    break;
                case 7:
                    out = "Week-end";
                    break;
                default:
                    out = "Invalid day";
                    break;
            }
        }

        { // (b) with if
            if (day < 1 || day > 7) {
                out = "Invalid day";
            } else if (day <= 5) {
                out = "Working day";
            } else {
                out = "Week-end";
            }
        }

        SimpleIO.display(out);
    }
}
