package s08;

public class Ex0805 {
    public static int[][] reduceImage(int[][] image) {
        int h = image.length;
        int l = image[0].length;
        int[][] reduced = new int[h/2][l/2];

        for (int i = 0; i < reduced.length; i++) {
            for (int j = 0; j < reduced[0].length; j++) {
                int pxl1 = image[i*2][j*2];
                int pxl2 = image[i*2][j*2+1];
                int pxl3 = image[i*2+1][j*2];
                int pxl4 = image[i*2+1][j*2+1];

                int moyenne = (pxl1+pxl2+pxl3+pxl4) / 4;

                reduced[i][j] = moyenne;
            }
        }

        return reduced;
    }

    public static void main(String[] args) {
        int[][] generated = new int[20][10];

        for(int i = 0; i < generated.length; i++) {
            for (int j = 0; j < generated[0].length; j++) {
                int rand = (int) (Math.random()*127);
                generated[i][j] = rand;
            }
        }

        int[][] reduced = reduceImage(generated);
        System.out.println(reduced.length);
    }
}
