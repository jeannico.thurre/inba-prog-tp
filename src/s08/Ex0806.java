package s08;

public class Ex0806 {
    public static void main(String[] Args) {
        boolean [] NombresPremiers = new boolean[200];
        NombresPremiers[0] = false;
        NombresPremiers[1] = false;

        for (int i = 0; i < NombresPremiers.length; i++) {
            NombresPremiers[i] = i != 0 && i != 1;
        }

        for (int i = 0; i < NombresPremiers.length; i++) {
            if(NombresPremiers[i]) {
                for (int j = 2; i*j < NombresPremiers.length; i++) {
                    NombresPremiers[i * j] = false;
                }
            }
        }
        for (int i = 0; i < NombresPremiers.length; i++) {
            System.out.println(i + " : " + NombresPremiers[i]);
        }
    }

}
