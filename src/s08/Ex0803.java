package s08;

public class Ex0803 {
    public static int[] combineArray(int[] arr1, int[] arr2) {
        if (arr1.length != arr2.length) {
            System.out.println("Arrays aren't the same size");
            return new int[0];
        }

        int size = arr1.length + arr2.length;
        int[] combined = new int[size];

        for(int i = 0; i < arr1.length; i++) {
            combined[i*2] = arr1[i];
            combined[i*2+1] = arr2[i];
        }

        return combined;
    }

    public static void main(String[] args) {
        int[] arr = combineArray(new int[]{1,3,5}, new int[]{2,4,6});
        System.out.println("");
    }
}
