package s08;

public class Ex0801 {
    public static void showContent(int[][] intArray) {
        String out = "";

        for(int i = 0; i < intArray.length; i++) {
            out += i + " : ";
            for(int j = 0; j < intArray[i].length; j++) {
                out += " " + intArray[i][j];
            }
            out += "\n";
        }

        System.out.println(out);
    }

    public static void main(String[] args) {
        showContent(new int[][]{ {1,2,3},{3,2,1},{4,5,6},{6,5,4} });
    }
}
