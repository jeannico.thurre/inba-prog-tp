package s08;

public class Ex0807 {
    public static int min(int[] arr) {
        int min = arr[0];

        for(int i = 1; i < arr.length; i++) {
            if (arr[i] < min) {
                min = arr[i];
            }
        }

        return min;
    }

    public static void main(String[] args) {
        System.out.println(min(new int[]{1,2,3,4,5,6,7}));
    }
}
